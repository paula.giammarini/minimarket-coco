# Python backend project - Minimarket API

 _Backend project developed in Python following Arzion's challenge specifications_

# First things first 📋

Before cloning the repository, it's recommended to create a virtual environment. That will create an isolated environment for the project. This means that the project can have its own dependencies, regardless of what dependencies every other project in your system has.

This is where you are going to install the requirements that make the project works correctly.

To do that, you only have to run these commands in the Terminal window:
```
virtualenv venv                      # this create the environment
. venv/bin/activate                  # this will activated the environment
```

# Clone the repository 📦

You can do it by following these commands:

```
git init
git clone https://gitlab.com/paula.giammarini/minimarket-coco.git
```
After that, install the requirements:

```
cd minimarket-coco
pip install -r requirements.txt      # this will install all you need for the project
deactivate                           # this will deactivatedthe environment
```

Before start testing the project, it is necessary to set up an environment variable. First of all, make sure your environment is deactivated. Then, you will have to modify the field venv/bin/activate by writting this at the end:
```
export FLASK_APP="run.py" 
```
After all that, you can activate the environment.


# Setup all data from a simple get  ⚙️

First, make sure your environment is activated.

Finally, the last thing you have do is enter in the project's folder and run migrations to have all the initial data in your database:
```
cd minimarket-coco
flask db upgrade
```
To run the app just run this command:
```
flask run
```
or try this one:
```
python run.py
```

Now, you have all you need to test the project as you wish. Enjoy!

:books: For more information visit: https://gitlab.com/paula.giammarini/minimarket-coco/-/wikis/Welcome-to-COCO's-minimarket

Visit swagger documentation (http://localhost:5000/swagger).
