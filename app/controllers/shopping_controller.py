from flask import request
from flask_restx import Resource
from app import api
from app.services.shopping_services import CartServices, VoucherServices
from app.schemas.shopping_schemas import item_schema_out, cart_schema
from app.error_handler.exceptions import (
    RequiredException,
    NotInHereException,
    NotStockException,
    NotApplyException
)


# Manage a simple virtual cart.
@api.route('/stores/<int:store_id>/carts/<int:cart_id>/items')
class CartResource(Resource):

    # Query all de items in the cart
    @api.doc(responses={200: 'Success', 400: 'Bad Request'})
    def get(self, store_id, cart_id):
        code = 200
        result = {}
        try:
            cart = CartServices().get(store_id, cart_id)
            result = cart_schema.dump(cart)
            return result
        except ValueError as e:
            code = 400
            result = str(e)
            return (result, code)
        except Exception:
            code = 404
            result = "Store or Cart Not Found"
            return (result, code)

    # Add items to the cart.
    @api.doc(params={'product': {'description': 'A product ID',
                                 'required': 'True', 'in': 'body',
                                 'example': '{"product": "8"}'}},
             responses={201: 'Success', 400: 'Bad Request'})
    def post(self, store_id, cart_id):
        code = 201
        try:
            params = request.get_json()
            item = CartServices().post(store_id, cart_id, params)
            result = item_schema_out.dump(item)
            return (result, code)
        except (RequiredException, NotStockException, ValueError) as e:
            code = 400
            result = str(e)
            return (result, code)
        except Exception:
            code = 404
            result = "Store, Cart or Product Not Found"
            return (result, code)

    # Remove items from the cart
    @api.doc(params={'product': {'description': 'A product ID',
                                 'type': 'integer', 'required': 'True'}},
             responses={204: 'Success', 400: 'Bad Request'})
    def delete(self, store_id, cart_id):
        code = 204
        try:
            params = request.args.to_dict()
            CartServices().delete(store_id, cart_id, params)
            return ({}, code)
        except (ValueError, RequiredException, NotInHereException) as e:
            code = 400
            result = str(e)
            return (result, code)
        except Exception:
            code = 404
            result = "Store, Cart or Product Not Found"
            return (result, code)


# Be able to check the validity of a Voucher code on said virtual cart.
# Calculate discounts and return both original and discounted prices.
@api.route('/stores/<int:store_id>/carts/<int:cart_id>/items/voucher')
class VoucherResource(Resource):

    @api.doc(params={
        'code': {'description': 'An alphanumeric code of a voucher',
                 'required': 'True', 'in': 'body',
                 'example': '{"code": "COCO21FF64RR11AS"}'}},
             responses={201: 'Success', 400: 'Bad Request'})
    def post(self, store_id, cart_id):
        code = 201
        try:
            params = request.get_json()
            cart = VoucherServices().post(store_id, cart_id, params)
            result = cart_schema.dump(cart)
            return (result, code)
        except (RequiredException, NotApplyException, ValueError) as e:
            code = 400
            result = str(e)
            return (result, code)
        except Exception:
            code = 404
            result = "Store, Cart or Voucher not Found"
            return (result, code)
