from flask import request
from flask_restx import Resource
from app import api
from app.error_handler.exceptions import NotStockException
from app.services.store_services import StoreServices
from app.services.product_services import (
    ProductServices,
    ProductStoreServices,
    ProductsListStoreServices
)
from app.schemas.stores_schemas import (
    stock_products_schema,
    store_schema
)
from app.schemas.products_schemas import (
    product_total_stocks_schema,
    product_schema
)


# Query all available products, across stores, with their total stock.
@api.doc(responses={200: 'Success'})
@api.route('/products')
class ProductsList(Resource):
    def get(self):
        code = 200
        products_list = ProductServices().get()
        result = product_total_stocks_schema.dump(products_list, many=True)
        result = sorted(result, key=lambda k: k['name'])
        return (result, code)  # Products order by name.


# Query available stores at a certain time in the day and return only
# those that apply
@api.doc(responses={200: 'Success', 400: 'Bad Request'},
         params={'day': {'description': 'The name of one day of the week'},
                 'time': {'description': 'An hour of the day (HH:MM:SS)'}})
@api.route('/stores')
class StoreAvailable(Resource):
    def get(self):
        code = 200
        result = {}
        try:
            params = request.args.to_dict()
            stores_list = StoreServices().get(params)
            result = store_schema.dump(stores_list, many=True)
            result = sorted(result, key=lambda k: k['name'])
            return (result, code)
        except ValueError:
            code = 400
            result = "Invalid parameters"
            return (result, code)


# Query available products for a particular store
@api.doc(responses={200: 'Success'})
@api.route('/stores/<int:store_id>/products')
class ProductsListStore(Resource):
    def get(self, store_id):
        code = 200
        result = {}
        try:
            stocks = ProductsListStoreServices().get(store_id)
            result = stock_products_schema.dump(stocks, many=True)
            result = sorted(result, key=lambda k: k['product']['name'])
            return (result, code)
        except Exception:
            code = 404
            result = "Store Not Found"
            return (result, code)


# Query if a product is available, at a certain store, and return that
# product's information
@api.doc(responses={201: 'Success', 400: 'Bad Request'})
@api.route('/stores/<int:store_id>/products/<int:product_id>')
class ProductsInfoStore(Resource):
    def get(self, store_id, product_id):
        code = 200
        result = {}
        try:
            product = ProductStoreServices().get(store_id, product_id)
            return (product_schema.dump(product), code)
        except NotStockException:
            code = 400
            result = "Product out of stock"
            return (result, code)
        except Exception:
            code = 404
            result = "Store or product Not Found"
            return (result, code)
