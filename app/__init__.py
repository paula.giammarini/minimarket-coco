import os

from flask import Flask
from flask_restx import Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate


DB_URI = "sqlite:///" + os.path.abspath(os.getcwd()) + "/databaseCOCO.db"

# Init app
app = Flask(__name__)
api = Api(app, title='Minimarket COCO API',
          description='A simple minimarket API for Arzion challenge',
          doc='/swagger',
          default='Endpoints',
          default_label='Things you can do with this app'
          )

app.config["SQLALCHEMY_DATABASE_URI"] = DB_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config.SWAGGER_UI_DOC_EXPANSION = 'list'

# Init DB
db = SQLAlchemy(app)
migrate = Migrate(app, db)


# Init Marshmallow
ma = Marshmallow(app)

from app.controllers.products_controller import *
from app.controllers.shopping_controller import *
from app.models.products_models import *
from app.models.stores_models import *
from app.models.shopping_models import *
