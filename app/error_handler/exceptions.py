class NotStockException(Exception):
    pass


class NotInHereException(Exception):
    pass


class RequiredException(Exception):
    pass


class NotApplyException(Exception):
    pass
