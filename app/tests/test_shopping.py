from app import app
from flask import json
from http import HTTPStatus
from app.models.stores_models import Store


class TestCartResource:
    """
    GET '/stores/<int:store_id>/carts/<int:cart_id>/items'

    Query all items in the cart of a particular store.

    In path: store_id must belong to an existing store
             cart_id must belong to an existing cart which must belog to
             the store that have the store_id
    """
    def test_get_items_in_cart_ok(self):
        store = Store.query.get(1)
        cart = store.carts[0]
        response = app.test_client().get(
            '/stores/' + str(store.id) + '/carts/' + str(cart.id) + '/items')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        assert 'total_amount' in data
        assert 'total_amount_discount' in data
        assert 'items' in data

    def test_get_items_in_cart_fail(self):
        store = Store.query.get(1)
        store_2 = Store.query.get(2)
        cart = store_2.carts[0]
        response = app.test_client().get(
            '/stores/' + str(store.id) + '/carts/' + str(cart.id) + '/items')
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "This cart does not belong to this store"
        response = app.test_client().get(
            '/stores/' + 'store1' + '/carts/' + str(cart.id) + '/items')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().get(
            '/stores/' + str(store.id) + '/carts/' + 'cart2' + '/items')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().get(
            '/stores/' + str(store.id) + '/carts/' + 'items')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().get(
            '/stores/' + '/carts/' + str(cart.id) + '/items')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
    """
    POST '/stores/<int:store_id>/carts/<int:cart_id>/items'

    Add an item in the cart of a particular store.

    In path: - store_id: must belong to an existing store
             - cart_id: must belong to an existing cart which must belog to
                        the store that have the store_id

    In body: - product: a product's id. This must exist.
    """

    def test_post_items_in_cart_ok(self):
        response = app.test_client().post(
            '/stores/1/carts/1/items',
            data=json.dumps(dict(product='9')),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())
        assert response.status_code == 201
        assert response.status_code == HTTPStatus.CREATED
        assert 'name' in data['product']
        assert 'price' in data['product']
        assert 'quantity' in data
        assert 'description' in data['product']

    def test_post_items_in_cart_fail(self):
        # Cart id:5 doesn't exist
        url = '/stores/' + str(1) + '/carts/' + str(5) + '/items'
        response = app.test_client().post(
            url,
            data=json.dumps(dict(product='9')),
            content_type='application/json',
        )
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        url = '/stores/' + str(1) + '/carts/' + str(1) + '/items'
        response = app.test_client().post(
            url,
            data=json.dumps(dict(product='example')),
            content_type='application/json',
        )
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        url = '/stores/' + str(1) + '/carts/' + str(3) + '/items'
        response = app.test_client().post(
            url,
            data=json.dumps(dict(product='9')),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "This cart does not belong to this store"
        url = '/stores/' + str(1) + '/carts/' + str(1) + '/items'
        response = app.test_client().post(
            url,
            data=json.dumps(dict(product=None)),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "Product required"
        response = app.test_client().post(
            url,
            data=json.dumps(dict(product='1')),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "Product out of stock"
        response = app.test_client().post(
            url,
            data=json.dumps(None),
            content_type='application/json',
        )
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "Product required"
    """
    DELETE '/stores/<int:store_id>/carts/<int:cart_id>/items'

    Add an item in the cart of a particular store.

    In path: - store_id: must belong to an existing store
             - cart_id: must belong to an existing cart which must belog to
                        the store that have the store_id

    Params: - product: a product's id. This must exist and must be already in
                       the cart.
    """

    def test_delete_items_in_cart_ok(self):
        response = app.test_client().delete(
            '/stores/1/carts/1/items?product=9')
        assert response.status_code == 204
        assert response.status_code == HTTPStatus.NO_CONTENT

    def test_delete_items_in_cart_fails(self):
        response = app.test_client().delete(
            '/stores/1/carts/8/items?product=50000')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().delete(
            '/stores/1/carts/8/items?product=9')
        data = json.loads(response.data.decode())
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().delete(
            '/stores/8/carts/1/items?product=9')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().delete(
            '/stores/1/carts/1/items?')
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "Product required"
        response = app.test_client().delete(
            '/stores/3/carts/1/items?product=9')
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "This cart does not belong to this store"
        response = app.test_client().delete(
            '/stores/1/carts/1/items?product=21')
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "The product is not in the cart"
