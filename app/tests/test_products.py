from app import app
from flask import json
from http import HTTPStatus


class TestProductsList:
    # GET /products
    def test_get_all_products(self):
        response = app.test_client().get('/products')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        assert len(data) == 22
        assert 'name' in data[0]
        assert 'price' in data[0]
        assert 'total_stock' in data[0]
        assert 'description' in data[0]


class TestsStoreAvailable:
    """
    GET /stores - Query all the available stores at certain time and day

    Params: day (optional) and time (optional)

    If the user don't send the params, these ones takes the values
    of the actual moment.

    For example: If you send the request without params on Tuesday at 12 am,
    the params will be:
    day = tuesday,
    time = 12:00:00

    """
    def test_get_stores_without_params(self):
        response = app.test_client().get('/stores')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK

    def test_get_stores_with_params_ok(self):
        response = app.test_client().get(
            '/stores?' + 'day=tuesday&time=12:00:00')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        assert len(data) == 2
        assert data[0]['name'] == 'COCO Bay'
        assert 'address' in data[0]
        assert 'logo' in data[0]
        assert 'id' in data[0]
        assert data[1]['name'] == 'COCO Downtown'
        assert 'address' in data[1]
        assert 'logo' in data[1]
        assert 'id' in data[1]
        response = app.test_client().get(
            '/stores?' + 'day=tuesday')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        response = app.test_client().get(
            '/stores?' + 'time=12:00:00')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        response = app.test_client().get(
            '/stores?' + 'time=None')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        response = app.test_client().get(
            '/stores?' + 'time=')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        response = app.test_client().get(
            '/stores?' + 'day=None')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        response = app.test_client().get(
            '/stores?' + 'day=')
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK

    # GET /stores fail
    def test_stores_with_params_fail(self):
        response = app.test_client().get(
            '/stores?' + 'day=tuesday&time=12:0:')
        data = json.loads(response.data.decode())
        assert data == "Invalid parameters"
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        response = app.test_client().get(
            '/stores?' + 'day=tuesday&time=test')
        data = json.loads(response.data.decode())
        assert data == "Invalid parameters"
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        response = app.test_client().get(
            '/stores?' + 'day=25&time=12:00:00')
        assert data == "Invalid parameters"
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST


class TestsProductsListStore:
    """
    GET /stores/<int:store_id>/products

    Query all the available products for certain store

    In path: store_id must belong to an existing store
    """
    def test_product_list_store_ok(self):
        response = app.test_client().get(
            '/stores/' + str(1) + '/products')
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        assert 'quantity' in data[0]
        assert 'product' in data[0]
        assert 'id' in data[0]
        assert 'price' in data[0]['product']
        assert 'name' in data[0]['product']
        assert 'description' in data[0]['product']
        assert 'name' in data[0]['product']
        assert 'categories' in data[0]['product']

    def test_product_list_store_fail(self):
        response = app.test_client().get(
            '/stores/' + 'fail' + '/products')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().get(
            '/stores/' + str(500) + '/products')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        response = app.test_client().get(
            '/stores/' + 'fail' + '/products')
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND


class TestsProductsInfoStore:
    """
    GET '/stores/<int:store_id>/products/<int:product_id>'

    Query if a product is available, at a certain store, and return that
    product's information

    In path: store_id must belong to an existing store and
             product_id must belong to an existing product
    """

    def test_get_available_product_ok(self):
        response = app.test_client().get(
            '/stores/' + str(1) + '/products/' + str(9))
        data = json.loads(response.data.decode())
        assert response.status_code == 200
        assert response.status_code == HTTPStatus.OK
        assert 'name' in data
        assert 'categories' in data
        assert 'description' in data
        assert 'price' in data

    def test_get_available_product_fail(self):
        response = app.test_client().get(
            '/stores/' + str(1) + '/products/' + str(900))
        data = json.loads(response.data.decode())
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        assert data == "Store or product Not Found"
        response = app.test_client().get(
            '/stores/' + str(1000) + '/products/' + str(9))
        data = json.loads(response.data.decode())
        assert response.status_code == 404
        assert response.status_code == HTTPStatus.NOT_FOUND
        assert data == "Store or product Not Found"
        response = app.test_client().get(
            '/stores/' + str(1) + '/products/' + str(1))
        data = json.loads(response.data.decode())
        assert response.status_code == 400
        assert response.status_code == HTTPStatus.BAD_REQUEST
        assert data == "Product out of stock"
