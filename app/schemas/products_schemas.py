from marshmallow import fields
from app import ma
from app.models.products_models import Product, Category


class CategorySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Category


class ProductSchema(ma.SQLAlchemyAutoSchema):
    categories = fields.Nested(CategorySchema, many=True)

    class Meta:
        model = Product


class ProductTotalStocksSchema(ma.SQLAlchemySchema):
    id = fields.Integer()
    name = fields.String()
    categories = fields.Nested(CategorySchema, many=True)
    description = fields.String()
    price = fields.Float()
    total_stock = fields.Method("get_total_stock")

    def get_total_stock(self, obj):
        total = 0
        for prod in obj.stocks:
            total += prod.quantity
        return total


product_schema = ProductSchema()
category_schema = CategorySchema()
product_total_stocks_schema = ProductTotalStocksSchema()
