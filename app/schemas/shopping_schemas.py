from marshmallow import fields
from marshmallow_sqlalchemy.fields import Nested
from app import ma
from app.models.shopping_models import Cart, Item, Voucher
from app.schemas.products_schemas import ProductSchema


class VoucherSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Voucher
        include_fk = True


class ItemSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Item
        include_fk = True
        load_instance = True


class ItemSchemaOut(ma.SQLAlchemySchema):
    quantity = fields.Integer()
    product = Nested(ProductSchema)

    class Meta:
        model = Item


class CartSchema(ma.SQLAlchemySchema):
    items = fields.Nested(ItemSchemaOut, many=True)
    total_amount_discount = fields.Float()
    total_amount = fields.Float()

    class Meta:
        model = Cart


cart_schema = CartSchema()
item_schema = ItemSchema()
item_schema_out = ItemSchemaOut()
