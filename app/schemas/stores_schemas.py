from marshmallow_sqlalchemy.fields import Nested
from app import ma
from app.models.stores_models import Store, Stock, WorkInformation, WeekDay
from app.schemas.products_schemas import ProductSchema


class StoreSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Store


class StockSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Stock
        include_fk = True


class WorkInformationSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = WorkInformation


class WeekDaySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = WeekDay


class StockProductSchema(ma.SQLAlchemyAutoSchema):

    class Meta:
        model = Stock
        exclude = ("store",)

    product = Nested(ProductSchema)


stock_products_schema = StockProductSchema()
store_schema = StoreSchema()
