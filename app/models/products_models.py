from app import db
from app.models.shopping_models import voucher_products

category_product = db.Table('category_product',
                            db.Column('product_id', db.Integer,
                                      db.ForeignKey('product.id')),
                            db.Column('category_id', db.Integer,
                                      db.ForeignKey('category.id')))


class Category(db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    product = db.relationship("Product", secondary=category_product,
                              backref="categories")


class Product(db.Model):
    __tablename__ = 'product'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    price = db.Column(db.Float)
    description = db.Column(db.String(200))
    voucher = db.relationship("Voucher", secondary=voucher_products,
                              backref="products")
    category = db.relationship("Category", secondary=category_product,
                               backref="products")
