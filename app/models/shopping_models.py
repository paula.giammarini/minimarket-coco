from app import db


cart_vouchers = db.Table('cart_vouchers',
                         db.Column('cart_id', db.Integer,
                                   db.ForeignKey('cart.id')),
                         db.Column('voucher_id', db.Integer,
                                   db.ForeignKey('voucher.id')))

voucher_products = db.Table('voucher_products',
                            db.Column('product_id', db.Integer,
                                      db.ForeignKey('product.id')),
                            db.Column('voucher_id', db.Integer,
                                      db.ForeignKey('voucher.id')))

voucher_days = db.Table('voucher_days',
                        db.Column('week_day_id', db.Integer,
                                  db.ForeignKey('week_day.id')),
                        db.Column('voucher_id', db.Integer,
                                  db.ForeignKey('voucher.id')))


class Cart(db.Model):
    __tablename__ = 'cart'
    id = db.Column(db.Integer, primary_key=True)
    store_id = db.Column(db.Integer, db.ForeignKey('store.id'))
    total_amount = db.Column(db.Float, default=0)
    total_amount_discount = db.Column(db.Float, default=0)
    store = db.relationship('Store', backref='carts')
    voucher = db.relationship("Voucher", secondary=cart_vouchers,
                              backref="carts")


class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer)
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'))
    cart = db.relationship('Cart', backref='items')
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product', backref='items')


class Voucher(db.Model):
    __tablename__ = 'voucher'
    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.String(16), unique=True)
    store_id = db.Column(db.Integer, db.ForeignKey('store.id'))
    store = db.relationship('Store', backref='vouchers')
    max_units = db.Column(db.Integer)
    min_units = db.Column(db.Integer)
    discount = db.Column(db.Float)
    initial_date = db.Column(db.Date)
    end_date = db.Column(db.Date)
    cart = db.relationship("Cart", secondary=cart_vouchers,
                           backref="vouchers")
    product = db.relationship("Product", secondary=voucher_products,
                              backref="vouchers")
    week_day = db.relationship("WeekDay", secondary=voucher_days,
                               backref="vouchers")
