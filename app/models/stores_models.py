import enum
from app import db
from app.models.shopping_models import voucher_days


class WeekDays(enum.Enum):
    monday = 'monday'
    tuesday = 'tuesday'
    wednesday = 'wednesday'
    thursday = 'thursday'
    friday = 'friday'
    saturday = 'saturday'
    sunday = 'sunday'


store_work = db.Table('store_work',
                      db.Column('store_id', db.Integer,
                                db.ForeignKey('store.id')),
                      db.Column('work_information_id', db.Integer,
                                db.ForeignKey('work_information.id')))

day_work = db.Table('day_work',
                    db.Column('week_day_id', db.Integer,
                              db.ForeignKey('week_day.id')),
                    db.Column('work_information_id', db.Integer,
                              db.ForeignKey('work_information.id')))


class WeekDay(db.Model):
    __tablename__ = 'week_day'
    id = db.Column(db.Integer, primary_key=True)
    day = db.Column(db.Enum(WeekDays))
    work_information = db.relationship("WorkInformation", secondary=day_work,
                                       backref="days")
    voucher = db.relationship("Voucher", secondary=voucher_days,
                              backref="days")


class WorkInformation(db.Model):
    __tablename__ = 'work_information'
    id = db.Column(db.Integer, primary_key=True)
    open_hour = db.Column(db.Time)
    close_hour = db.Column(db.Time)
    day = db.relationship("WeekDay", secondary=day_work,
                          backref="work_informations")
    store = db.relationship("Store", secondary=store_work,
                            backref="work_informations")


class Store(db.Model):
    __tablename__ = 'store'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200))
    address = db.Column(db.String(200))
    logo = db.Column(db.String(200))
    work_information = db.relationship("WorkInformation", secondary=store_work,
                                       backref="stores")


class Stock(db.Model):
    __tablename__ = 'stock'
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer)
    store_id = db.Column(db.Integer, db.ForeignKey('store.id'))
    store = db.relationship('Store', backref='stocks')
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product', backref='stocks')
