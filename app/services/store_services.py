from app.models.stores_models import Store, WorkInformation, WeekDay
from datetime import datetime


class StoreServices:

    # This method return the name of the day of the week.
    def __dayNameFromWeekday(self, weekday):
        week = ["monday", "tuesday", "wednesday", "thursday", "friday",
                "saturday", "sunday"]
        return week[weekday]

    # This method return True if the params weekday is valid.
    def __isAWeekday(self, weekday):
        week = ["monday", "tuesday", "wednesday", "thursday", "friday",
                "saturday", "sunday"]
        if weekday in week:
            return True
        else:
            return False

    """
    This method returns all available stores, at certain day and time.
    The params day and time are optionals.
    (If they are null, they will take values of the actual moment)
    """
    def get(self, params):
        stores_list = []
        # If the day is null, the param takes the value of the name of
        # the day of today.
        if 'day' not in params or params['day'] == '' or (
                params['day'] == 'None'):
            today = datetime.today().weekday()
            params['day'] = self.__dayNameFromWeekday(today)
        # If the day is not null, it must be a valid value.
        elif not self.__isAWeekday(params['day']):
            raise ValueError
        # If time is null, the param takes the value of the actual hour.
        if 'time' not in params or params['time'] == '' or (
                params['time'] == 'None'):
            params['time'] = datetime.today().time()
        else:
            # If the time is not null, it must be a valid value.
            try:
                datetime.strptime(params['time'], '%H:%M:%S').time()
            except (ValueError, TypeError, SyntaxError, AttributeError):
                raise ValueError
        """
        The params are valid.
        So all available stores, at that day and time, are searched.
        """
        works_info = WorkInformation.query.filter(
            WorkInformation.open_hour < params['time'],
            WorkInformation.close_hour > params['time'],
        )
        day = WeekDay.query.filter(WeekDay.day == params['day']).first()
        for info in works_info:
            if day in info.days:
                for s in info.stores:
                    store = Store.query.get(s.id)
                    if store not in stores_list:
                        stores_list.append(store)
        return stores_list
