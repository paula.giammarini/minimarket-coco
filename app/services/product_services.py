from app.models.products_models import Product
from app.models.stores_models import Stock, Store
from app.error_handler.exceptions import NotStockException


class ProductServices:
    """
    This method returns all the products that are available.
    (in other words: the products which have stock in any store)
    """
    def get(self):
        products_list = []
        for s in Stock.query.filter(Stock.quantity != 0):
            product = Product.query.get(s.product_id)
            if product not in products_list:
                products_list.append(product)
        return products_list


class ProductStoreServices:
    """
    This method returns a product, if it has stock in certain store.
    """
    def get(self, store_id, product_id):
        if Store.query.get(store_id) is None:
            raise Exception
        if Product.query.get(product_id) is None:
            raise Exception
        stock = Stock.query.filter(Stock.store_id == store_id,
                                   Stock.quantity != 0,
                                   Stock.product_id == product_id)
        if stock.count() != 0:
            return Product.query.get(product_id)
        else:
            raise NotStockException


class ProductsListStoreServices:
    """
    This method returns all the products which have stock in certain store.
    """
    def get(self, id):
        if Store.query.get(id) is None:
            raise Exception
        else:
            stocks = Stock.query.filter(Stock.store_id == id,
                                        Stock.quantity != 0)
        return stocks
