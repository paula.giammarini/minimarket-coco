from datetime import datetime
from app import db
from app.models.shopping_models import Cart, Item, Voucher
from app.models.products_models import Product
from app.models.stores_models import Stock, Store, WeekDay
from app.schemas.shopping_schemas import item_schema
from app.error_handler.exceptions import (
    RequiredException,
    NotInHereException,
    NotStockException,
    NotApplyException
)


class CartServices:
    """
    This private method validates the data.
    """
    def __validate_data(self, store_id, cart_id, params):
        if (params is None or ('product' not in params) or (
                params['product'] is None)):
            raise RequiredException("Product required")
        elif (Product.query.get(params['product']) is None or (
                Store.query.get(store_id) is None) or (
                    Cart.query.get(store_id) is None)):
            raise Exception
        elif store_id != Cart.query.get(cart_id).store_id:
            raise ValueError("This cart does not belong to this store")
        else:
            return True

    """
    This method returns a cart of certain store.
    """
    def get(self, store_id, cart_id):
        if Store.query.get(store_id) is None:
            raise Exception
        elif Cart.query.get(store_id) is None:
            raise Exception
        elif store_id == Cart.query.get(cart_id).store_id:
            return Cart.query.get(cart_id)
        else:
            raise ValueError("This cart does not belong to this store")

    """
    This method adds a item of a product into a cart, and return that item.
    """
    def post(self, store_id, cart_id, params):
        if self.__validate_data(store_id, cart_id, params):
            # To add an item into a cart, the product must have stock in that
            # store
            product_stock = Stock.query.filter(
                Stock.product_id == params['product'],
                Stock.store_id == store_id).first()
            if product_stock is not None:
                if product_stock.quantity > 0:
                    # The product has stock.
                    cart = Cart.query.get(cart_id)
                    product = Product.query.get(params['product'])
                    # Is the product already in the cart?
                    in_cart = Item.query.filter(
                        Item.cart_id == cart_id,
                        Item.product_id == params['product'])
                    if in_cart.count() != 0:
                        # if it is, just add one.
                        in_cart.first().quantity += 1
                    else:
                        # if it isn't, then create one.
                        values = {
                            "cart_id": cart_id,
                            "product_id": str(params['product']),
                            "quantity": str(1)
                        }
                        schema_data = item_schema.load(values,
                                                       session=db.session)
                        db.session.add(schema_data)
                        in_cart = Item.query.filter(
                            Item.cart_id == cart_id,
                            Item.product_id == params['product'])
                    # Update the total amounts and the stock of the product
                    product_stock.quantity -= 1
                    cart.total_amount += product.price
                    cart.total_amount_discount += product.price
                    db.session.commit()
                    return in_cart.first()
                else:
                    raise NotStockException("Product out of stock")

    """
    This method removes a item of a product of a cart.
    """
    def delete(self, store_id, cart_id, params):
        if self.__validate_data(store_id, cart_id, params):
            product_stock = Stock.query.filter(
                Stock.product_id == params['product'],
                Stock.store_id == store_id).first()
            # To take remove the item, the product must be in the cart
            in_cart = Item.query.filter(Item.cart_id == cart_id,
                                        Item.product_id == params['product'])
            cart = Cart.query.get(cart_id)
            product = Product.query.get(params['product'])
            if in_cart.count() != 0:
                # Remove 1 item of that product
                # If there is only 1 just delete the item.
                if in_cart.first().quantity == 1:
                    db.session.delete(Item.query.get(in_cart.first().id))
                else:
                    # If there are more than 1 just take off one.
                    in_cart.first().quantity -= 1
                # Update the stock of that product and total amounts
                product_stock.quantity += 1
                cart.total_amount -= product.price
                cart.total_amount_discount -= product.price
                db.session.commit()
            else:
                raise NotInHereException("The product is not in the cart")


class VoucherServices:
    """
    This private method returns the name of the day of the week.
    """
    def __dayNameFromWeekday(self, weekday):
        week = ["monday", "tuesday", "wednesday", "thursday", "friday",
                "saturday", "sunday"]
        return week[weekday]

    """
    This private method validates the data.
    """
    def __validate_data(self, store_id, cart_id, params):
        if (params is None or 'code' not in params or params['code'] is None):
            raise RequiredException("Code required")
        elif Voucher.query.filter(
                Voucher.code == params['code']).first() is None:
            raise Exception
        elif Store.query.get(store_id) is None:
            raise Exception
        elif Cart.query.get(store_id) is None:
            raise Exception
        elif store_id != Cart.query.get(cart_id).store_id:
            raise ValueError("This cart does not belong to this store")
        elif store_id != Voucher.query.filter(
                Voucher.code == params['code']).first().store_id:
            raise ValueError("This voucher does not belong to this store")
        else:
            return True

    """
    This private method validates the dates and cart. In other words returns
    if a voucher can be applied.
    """
    def __can_apply(self, cart, voucher):
        if cart in voucher.carts:  # The voucher has been applied before.
            return False
        today = datetime.today().date()
        if today > voucher.end_date or today < voucher.initial_date:
            return False
        today = self.__dayNameFromWeekday(today.weekday())
        today = WeekDay.query.filter(WeekDay.day == today).first()
        if not (today in voucher.days):
            return False
        else:
            return True

    """
    This method applie a voucher to a certain cart. Calculates the discounts
    and update the amounts. The method returns a car with his total amounts and
    items.
    """

    def post(self, store_id, cart_id, params):
        if self.__validate_data(store_id, cart_id, params):
            # The params are valid.
            valid_product = False
            voucher = Voucher.query.filter(
                Voucher.code == params['code']).first()
            cart = Cart.query.get(cart_id)
            if self.__can_apply(cart, voucher):
                # The voucher can be applied to this cart.
                for item in cart.items:
                    product = Product.query.get(item.product_id)
                    if product in voucher.products and (
                            item.quantity >= voucher.min_units):
                        """
                        The voucher can be applied to this product and this
                        quantity of products.
                        """
                        valid_product = True
                        min_units = voucher.min_units
                        d = min_units * voucher.discount * product.price
                        # How many times applied the discount?
                        if (item.quantity > voucher.max_units):
                            units = voucher.max_units // min_units
                        else:
                            units = item.quantity // min_units
                        # Update total amount discount
                        cart.total_amount_discount -= (d * units)
                        db.session.commit()
            else:
                raise NotApplyException("Voucher not applied")
            if not valid_product:
                raise NotApplyException("No product valid for the voucher")
            else:
                # The voucher has benn applied.
                voucher.carts.append(cart)
                cart.vouchers.append(voucher)
                db.session.commit()
                return (cart)
        return None
